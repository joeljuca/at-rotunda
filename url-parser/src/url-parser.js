/** Shorthand for RegExp#toString() and String#slice() */
const sliceRegExp = (regex, start, end) =>
  String.prototype.slice.call(regex.toString(), start, end);

const REGEX_PATTERN = /^(\/:?[\w-%]+)+$/;
const REGEX_PATTERN_PARTS = new RegExp(
  `${sliceRegExp(REGEX_PATTERN, 2, -3)}`,
  "g"
);

const REGEX_URL_PATH = /(\/[\w-%]+)+/g;
const REGEX_URL_PATH_PARTS = new RegExp(
  `${sliceRegExp(REGEX_URL_PATH, 1, -3)}`,
  "g"
);
const REGEX_URL_QUERY = /(&?[\w-%]+(=[\w-%]*)?)+/g;
const REGEX_URL_QUERY_PARTS = new RegExp(
  `${sliceRegExp(REGEX_URL_QUERY, 1, -3)}`,
  "g"
);
const REGEX_URL = new RegExp(
  `^${sliceRegExp(REGEX_URL_PATH, 1, -2)}(\\?${sliceRegExp(
    REGEX_URL_QUERY,
    1,
    -2
  )})?$`
);

const validatePattern = pattern => {
  if (!REGEX_PATTERN.test(pattern)) {
    throw new SyntaxError("Invalid pattern");
  }
};

const validateUrl = (config = {}, url) => {
  if (!REGEX_URL.test(url)) {
    throw new SyntaxError("Invalid URL");
  }

  const urlParts = url.match(REGEX_URL_PATH_PARTS);

  if (config.length !== urlParts.length) {
    throw new SyntaxError("URL longer than pattern");
  }
};

const configFromPattern = pattern => {
  return (
    validatePattern(pattern),
    pattern
      .match(REGEX_PATTERN_PARTS)
      .filter(Boolean)
      .map(fragment => {
        const type = fragment.startsWith("/:") ? "variable" : "literal";
        const value = fragment.match(/^\/:?(.*)$/)[1];

        return { type, value: decodeURIComponent(value) };
      })
  );
};

const urlParser = (pattern, url) => {
  validatePattern(pattern);

  if (url === undefined) return urlParser.bind(null, pattern);

  const config = configFromPattern(pattern);
  const urlParts = url.match(REGEX_URL_PATH_PARTS);

  validateUrl(config, url);

  // First, extract data from URL's query
  const queryValues = (!url.includes("?")
    ? []
    : url.slice(url.indexOf("?") + 1).split("&")
  ).map(pair => pair.split("="));

  // Then, extract data from URL's path
  let urlValues = config
    .map((entry, index) =>
      entry.type !== "variable" ? null : [entry.value, urlParts[index].slice(1)]
    )
    .filter(Boolean);

  const finalValues = [...queryValues, ...urlValues]
    // .map(([key, val]) => ({ [key]: val === undefined ? true : val }))
    .reduce(
      (values, [key, val]) => ({
        ...values,
        [key]: val
      }),
      {}
    );

  // console.log({ pattern, url, finalValues });
  return finalValues;
};

export default urlParser;
