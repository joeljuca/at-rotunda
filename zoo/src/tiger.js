import Animal from "./animal";

export default class Tiger extends Animal {
  constructor() {
    super();

    this.sound = "grrr";
  }
}
