import Animal from "../animal";
import Lion from "../lion";

describe("Lion", () => {
  it("is a class", () => {
    expect(Lion).toBeInstanceOf(Function);
  });

  it("can be instantiated", () => {
    expect(() => new Lion()).not.toThrow();
  });

  describe("#speak()", () => {
    const speak = Lion.prototype.speak;

    it("is a function", () => {
      expect(speak).toBeInstanceOf(Function);
    });

    // Since Lion inherits speak() from Animal, its coverage is enough
    it("is the same of Animal#speak", () => {
      expect(Lion.prototype.speak).toEqual(Animal.prototype.speak);
    });
  });
});
