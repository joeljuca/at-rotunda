import Animal from "../animal";

describe("Animal", () => {
  it("is a class", () => {
    expect(Animal).toBeInstanceOf(Function);
  });

  it("should not be instantiated", () => {
    expect(() => new Animal()).toThrow();
  });

  describe("#speak()", () => {
    const speak = Animal.prototype.speak;

    it("is a function", () => {
      expect(speak).toBeInstanceOf(Function);
    });

    it("returns a string", () => {
      expect(typeof speak.call({}, "hello world")).toBe("string");
    });

    it("uses a property `sound`", () => {
      const sound = "asdfasdf";
      expect(speak.call({ sound }, "hello world")).toContain(sound);
    });

    it("puts `sound` after every word of first parameter", () => {
      expect(speak.call({ sound: "asdf" }, "hello world")).toEqual(
        "hello asdf world asdf"
      );
    });
  });
});
