import Animal from "./animal";

export default class Lion extends Animal {
  constructor() {
    super();

    this.sound = "roar";
  }
}
