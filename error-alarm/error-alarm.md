# Error Alarm Exercise

I would write a Node.js package that would run both as library and stand-alone command-line program. As a library, it could be installed into a given event emitter, where it would ingest events (error logs) and act upon it. As a command-line program, it would watch a given log file, ingesting future entries that would eventually be written to it, then acting whenever needed.

The library and command-line "modes" would share most of the codebase, differing only on how events would be consumed. Events would be stored on RAM for usability purposes, but an external component (eg: Redis) could be used as cache storage.

## The logic

I would implement a [TLRU cache](<https://en.wikipedia.org/wiki/Cache_(computing)#Time_aware_least_recently_used_(TLRU)>) to store error events on it. A TTL (Time-to-Live) configuration could be provided, and there will be a default value of one minute. Whenever an error event occurs, the total number of events would then be checked, so if it's bigger than a given threshold a reaction (email notification) would be fired.

A minimum time between two reactions would be respected, to avoid firing multiple reactions sequentially, flooding the administrator's communication channel (mail inbox).

## The components

Although not complete, the following components illustrate how it could be built with real world JavaScript components.

> By component, I mean functional pieces that tackle different problems. In practice, they will often be JavaScript modules, but by "component" I mean an abstract resource that solves a problem, not a concrete module, function or class.

### The TLRU component

The _Time-aware Least Recently Used_ cache component would be responsible to store entries (error events), get rid of expired ones, and return remaining entries when requested. It will store recent error events, relevant to the decision of firing or not a notification.

It would work somehow like that:

```js
const tlru = new TLRU({ ttl: 30 /* time in seconds */ });

// Entries come from a given file (eg: /var/log/apache2/error.log).
// Line breaks ("\n") delimits error entries.
// Entries are added with the configured TTL of 60 seconds
tlru.add("2019-12-09T22:00:00.416Z Something somewhere went wrong very badly");

// TLRU would have a Set-compatible API
console.log([...tlru].length); // => 1

setTimeout(
  () => console.log([...tlru].length), // => 0
  30001
);
```

This would be how a `tlru.js` would look like:

```js
// src/tlru.js
import TLRU from "@rotundasoftware/tlru";

const tlru = new TLRU({ ttl: 60 });

export default tlru;
```

### The Notifier component

Responsible to notify someone about something, the Notifier component knows how to send a message to someone (eg: an email to someone's inbox). It also make sure a recipient never receives two notifications within less than a minimum interval.

It's not concern on the whys, but the hows of a notification.

```js
// src/notifier.js
import Notifier from "@rotundasoftware/notifier";

const notifier = new Notifier({
  interval: 60, // Minimum interval between notifications. Time in seconds.
  recipient: "root@localhost.local"
});

export default notifier;
```

### The EventWatcher component

The EventWatcher component would take care of ingesting error events - either by an event emitter or by watching the file system for changes on a given file. It would keep track of recent error events (by using the TLRU component), and decide when a notification must be sent (using the Notifier component).

```js
// src/event-watcher.js
import notifier from "./notifier";
import tlru from "./tlru";

const eventWatcher = new EventWatcher({
  // store would accept a Set-compatible API
  store: tlru
});

eventWatcher.onExceed(async () => {
  const message = `A concerning amount of errors has been logged recently. You might want to check the system to see what is going on.

Below is a snapshot preview of recent logged errors:

${[...tlru].map(entry => "- " + entry).join("/n")}`;

  // Notifies the given recipient (notification logic is handled internally)
  await notifier.notify(message);

  // Deletes all error entries after a successful notification
  for (entry in tlru) tlru.delete(entry);
});

if (require.main === module) {
  // TODO: parse CLI arguments and use it as command-line program
}

// Otherwise, it can be used as Node.js library
export default eventWatcher;
```

## Final considerations

- [The Twelve-Factor App](https://12factor.net) is a great set of best practices to develop software that's easy to use, test, deploy and debug
- Writing code that's analogous to the environment where it will run is difficult. Therefore, a simpler implementation of such solution would depend on the codebase it would be used, the existing libraries and resources (databases, queues, etc.) already available to a given system. I assumed a very abstract implementation.
